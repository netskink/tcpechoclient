//
//  str_cli.c
//  tcpechoclient
//
//  Created by John Fred Davis on 3/26/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#include "unp.h"
#include "wrapsock.h"
#include "wrapunix.h"
#include "error.h"
#include "str_cli.h"

void str_cli(FILE *fp, int sockfd) {
    char sendline[MAXLINE], recvline[MAXLINE];
    
    while (Fgets(sendline, MAXLINE, fp) != NULL ) {
        Writen(sockfd,sendline, strlen(sendline));
        
        if (Readline(sockfd, recvline, MAXLINE) == 0) {
            err_quit("str_cli: server terminated prematurely");
        }
        
        Fputs(recvline, stdout);
    }
}