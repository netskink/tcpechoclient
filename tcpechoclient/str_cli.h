//
//  str_cli.h
//  tcpechoclient
//
//  Created by John Fred Davis on 3/26/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef str_cli_h
#define str_cli_h

void str_cli(FILE *fp, int sockfd);


#endif /* str_cli_h */
