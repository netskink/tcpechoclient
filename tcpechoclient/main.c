//
//  main.c
//  tcpechoclient
//
//  Created by John Fred Davis on 3/17/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#include "unp.h"
#include "wrapsock.h"
#include "wrapunix.h"
#include "str_cli.h"
#include "error.h"


int main(int argc, const char * argv[]) {

    
    int sockfd;
    struct sockaddr_in servaddr;
    
    if (argc !=2) {
        err_quit("usage: tcpcli <IPaddress>");
    }
    
    sockfd = Socket(AF_INET, SOCK_STREAM, 0);
    
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(SERV_PORT);
    
    Inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
    
    Connect(sockfd, (SA *) &servaddr, sizeof(servaddr));
    
    str_cli(stdin, sockfd);         // do it all
    
    return 0;
}
